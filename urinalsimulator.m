clear
close all

Qmax = Inf;

%%   FIRST SERIES OF PLOTS
N = 2:1:10;
for kk=1:length(N)
    if kk==1
        x = 16;
    else
        x = F(kk-1);
    end
    [t,Q] = bath(N(kk),1/x,Qmax);
    while all(Q<=5)
        x=x-1;
        [t,Q,sumO] = bath(N(kk),1/x,Qmax);
    end
    F(kk)=x+1;
end
plot(N,F,'ro');
hold on
F = 3:1:17;
worst = ceil(42.*1./F);
best = ceil(21.*1./F);
plot(ceil(42.*1./F),F,'k.');
plot(ceil(21.*1./F),F,'k.');
uistack(fill([worst, fliplr(best)],[F, fliplr(F)],[0.5 0.5 0.5],'FaceAlpha',0.2,'EdgeColor',[0.5 0.5 0.5]),'bottom');
set(gca,'YDir','reverse','FontSize',12)
xlabel('Number of urinals [-]');
ylabel('Avg. time spacing btw. each person [s]');
set(gcf,'Position',[680   750   411   348]);

%% SECOND SERIES OF PLOTS
% [t,Q, sumO] = bath(3,1/9,Qmax);
% plot(t./60,Q,'ro');
% xlabel('Time [min]');
% ylabel('Queuing people [-]');
% set(gca,'FontSize',12)
% set(gcf,'Position',[680   750   411   348]);
% % ylim([0 5]);

%% THIRD SERIES OF PLOTS 
% avgF = 1/9;
% mu = 2*60;
% sigma =0.93;
% f = @(x)1./(sigma.*sqrt(2*pi)).*exp(-((log(x)-log(mu))./sigma).^2./2);
% 
% F = getFlow(f,avgF,15*60);
% 
% N = 3;
% 
% [t,Q, sumO] = bath(N,F,Qmax);
% yyaxis left
% plot(t./60,Q,'o','Color','r','MarkerSize',3)
% set(gca,'YColor',[0.1500    0.1500    0.1500])
% ylabel('Queuing people [-]');
% yyaxis right
% set(gca,'YColor',[0.1500    0.1500    0.1500])
% % plot(linspace(0.2,15*60,1000)./60,f(linspace(0.2,15*60,1000)),'k','LineWidth',1);
% plot(t(find(F==1))./60,[0,1./diff(find(F==1))],'k.')
% hold on
% line([0 20],[N/42, N/42],'Color',[0.5 0.5 0.5],'LineStyle','--');
% line([0 20],[N/21, N/21],'Color',[0.5 0.5 0.5],'LineStyle','--');
% ylabel('Instantaneous people inflow [p/s]');
% xlabel('Time [min]');
% set(gca,'FontSize',12)
% set(gcf,'Position',[680   750   411   348]);
% xlim([0 20]);

%% This function given a continuous inflow function returns a discrete time
% resolved series of inflow frequency whose overall average is the same as
% the average integral of the continuous function
function F = getFlow(funh,avgF,time_span)

p0 = 1.2;
p=p0;
P = 1.1;
I = 1.5;
e = 0;

while 1
    F = 1./round(1./funh(1:1:time_span))./sum(1./round(1./funh(1:1:time_span))).*(avgF*time_span).*p;
    
    k = 0;
    z = 1;
    for ii=1:length(F)
        k = k+F(ii);
        if k>1
            baseF = zeros(1,ii-z+1);
            baseF(randi(ii-z+1)) = 1;
            F(z:ii)=baseF;
            z=ii+1;
            k=0;
        end
    end
    F(and(F>0,F<1))=0;
    
    if abs(sum(F)/time_span-avgF)<2E-3
        break;
    else
        e(end+1) = avgF-sum(F)/time_span;
        p=p0+P*e(end)+I*sum(e);
    end
end

F = horzcat(F,zeros(1,5000));
end

function [t,Q, sumO] = bath(N,Fstar,Qmax)

suscept = 0.5;

% Start with all spots free
O = zeros(1,N);
% Cumulative filling
sumO = O;

% Variable to count how much time a person spent already
count = zeros(1,N);
% Starting time
t = 1;
% Simulation time 20 min
t_end = 20*60;
% Time resolution
dt = 1;
% Queue initially empty
Q = 0;

% Compute when people will arrive by randomly picking a time for each
% amount of time per person, e.g. for 1 person each 5 seconds we determine
% when this person is gonna show up within these 5 seconds, and so for all
% the simulation time.
if length(Fstar)>1
    F = Fstar;
else
    F = [];
    for ii=1:ceil(t_end/(1/Fstar))
        baseF = zeros(1,1/Fstar);
        baseF(randi(1/Fstar)) = 1;
        F = horzcat(F,baseF);
    end
end

% Normal time to pee
tPiss_normal = 21;
% Retarded time due to pressure
tPiss_ret = tPiss_normal*2;
% Time from the start that exposes to a sensitivity
tPiss_sens = 5;

% Time that a person need to end urination
time_needed = zeros(1,N);

while t(end) < t_end
    % First check whether there are people who wanna enter
    if F(t(end))==1
        % Add them to the queue
        if Q(end)<Qmax
            Q(end)=Q(end)+1;
        else
            %
        end
    end

    % Go on if there are no spots available
    if ~all(O==1)
        % If there are spots available and somebody wants to enter
        while Q(end)>0
            for kk=1:N
                % If the spot is occupied no chances you can got there
                if O(kk)==1
                    p(kk)=Inf;
                else
                    % Find the other occupied spots
                    occpos = find(O==1);
                    p(kk) = 0;
                    if ~isempty(occpos)
                        for jj=1:length(occpos)
                            % Sum the contribution of the other presences
                            p(kk)=p(kk)+1/abs(occpos(jj)-kk);
                        end
                    else
                        p(kk) = 0;
                    end
                end
            end
            
            % This gives you the index of the preferential spot to be
            % filled
            % If all the spots are free go on one edge
            if all(p==0)
                I = 1+(randi(2)-1)*(N-1);
            else
                % Pick one among the minima based on the distance
                [~,I] = min(p);
                allI = find(p==p(I));
                I = allI(randi(length(allI)));
            end
            
            % The spot is filled
            O(I) = 1;
            % Keep track of the filling
            sumO(I) = sumO(I)+1;
            % And one person less is waiting
            Q(end) = Q(end)-1;
            
            % Now we determine how much the guy needs to piss based on the
            % fact that he may have neighbours, and as well we take into
            % account that the neighbours may take longer because of the
            % newcome
            if I==1 % left edge
                if O(2)==1 % if there is someone next
                    if rand(1)<suscept % if the guy is susceptible 
                        time_needed(1) = tPiss_ret; % takes longer
                    else % otherwise
                        time_needed(1) = tPiss_normal; % takes normal
                    end
                    % if the guy next is susceptible
                    if and(count(2)<tPiss_sens,rand(1)<suscept)
                        time_needed(2) = tPiss_ret; % takes longer
                    end % otherwise leave things as they are
                else % nobody is next so all normal
                    time_needed(1) = tPiss_normal;
                end
            elseif I==N % right edge - SAME LOGIC AS BEFORE
                if O(end-1)==1
                    if rand(1)<suscept
                        time_needed(end) = tPiss_ret;
                    else
                        time_needed(end) = tPiss_normal;
                    end
                    if and(count(end-1)<tPiss_sens,rand(1)<suscept)
                        time_needed(end-1) = tPiss_ret;
                    end
                else
                    time_needed(end) = tPiss_normal;
                end
            else % middle case - SAME LOGIC AS BEFORE
                if or(O(I-1)==1,O(I+1)==1)
                    if rand(1)<suscept
                        time_needed(I) = tPiss_ret;
                    else
                        time_needed(I) = tPiss_normal;
                    end
                    if all([O(I-1)==1,count(I-1)<tPiss_sens,rand(1)<suscept])
                        time_needed(I-1) = tPiss_ret;
                    end
                    if all([O(I+1)==1,count(I+1)<tPiss_sens,rand(1)<suscept])
                        time_needed(I+1) = tPiss_ret;
                    end
                else
                    time_needed(I) = tPiss_normal;
                end
            end % if I==1
            
            % If the spots are now all full just exit the loop
            if all(O==1)
                break;
            end
        end % end while
    end % end ~all(O==1)
        % else nothing happens, i.e. the queue does not get reduced
    
    % Save the value
    Q(end+1)=Q(end);
    
    % We increase the time
    t(end+1) = t(end)+dt;
    % We increase the time count for each person
    count = count + O.*dt;
    
    % Make people leave if they can
    O(and(count>0,count>=time_needed))=0;
    % Reset the count for them
    count(and(count>0,count>=time_needed))=0;
    
end % while t<t_end
end